package com.example.demo;

public enum RoundResult {
    WIN,
    TIE,
    CONTINUE
}
