package com.example.demo;

public class WinConditionChecker {
    public RoundResult getRoundResult(char[][] board, int lastShootX, int lastShootY) {
        char symbol = board[lastShootY][lastShootX];
        assert symbol == 'x' || symbol == 'o';

        // check horizontal win
        for (int x = 0; x < board.length;) {
          if (board[lastShootY][x] != symbol) {
              x++;
              break;
          }
          x++;
          if (x == board.length) {
             return RoundResult.WIN;
          }
        }

        // check vertical
        for (int y = 0; y < board.length;) {
            if (board[y][lastShootX] != symbol) {
                y++;
                break;
            }
            y++;

            if (y == board.length) {
                return RoundResult.WIN;
            }
        }

        // check diagonal left
        for (int x = 0, y = 0; x < board.length && y < board.length;) {
            if (board[y][x] != symbol) {
                x++; y++;
                break;
            }
            x++; y++;
            if (x == board.length && y == board.length) {
                return RoundResult.WIN;
            }
        }

        // check diagonal right
        for (int x = 0, y = board.length-1; x < board.length && y >= 0;) {
            if (board[y][x] != symbol) {
                x++; y--;
                break;
            }
            x++; y--;
            if (x == board.length && y == -1) {
                return RoundResult.WIN;
            }
        }

        // check for tie
        if (checkTie(board)) {
            return RoundResult.TIE;
        }

        return RoundResult.CONTINUE;
    }

    private boolean checkTie(char[][] board) {
        for (int y = 0; y < board.length; y++) {
            for (int x = 0; x < board.length; x++) {
                if (board[y][x] == ' ') {
                    return false;
                }
            }
        }

        return true;
    }
}
