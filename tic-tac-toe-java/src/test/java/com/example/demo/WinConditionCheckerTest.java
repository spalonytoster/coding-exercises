package com.example.demo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static com.example.demo.RoundResult.*;
import static org.assertj.core.api.Assertions.assertThat;

class WinConditionCheckerTest {

    private final WinConditionChecker winConditionChecker;

    WinConditionCheckerTest() {
        winConditionChecker = new WinConditionChecker();
    }

    @Test
    void testWinHorizontal() {
        char[][] board = {
                {'x', 'x', 'x'},
                {' ', ' ', ' '},
                {' ', ' ', ' '}
        };
        int lastShootX = 1;
        int lastShootY = 0;

        RoundResult roundResult = winConditionChecker.getRoundResult(board, lastShootX, lastShootY);

        assertThat(roundResult).isEqualTo(WIN);
    }

    @Test
    void testWinVertical() {
        char[][] board = {
                {'x', ' ', ' '},
                {'x', ' ', ' '},
                {'x', ' ', ' '}
        };
        int lastShootX = 0;
        int lastShootY = 1;

        RoundResult roundResult = winConditionChecker.getRoundResult(board, lastShootX, lastShootY);

        assertThat(roundResult).isEqualTo(WIN);
    }

    @Test
    void testWinDiagonalLeft() {
        char[][] board = {
                {'x', ' ', ' '},
                {' ', 'x', ' '},
                {' ', ' ', 'x'}
        };
        int lastShootX = 1;
        int lastShootY = 1;

        RoundResult roundResult = winConditionChecker.getRoundResult(board, lastShootX, lastShootY);

        assertThat(roundResult).isEqualTo(WIN);
    }

    @Test
    void testWinDiagonalRight() {
        char[][] board = {
                {' ', ' ', 'x'},
                {' ', 'x', ' '},
                {'x', ' ', ' '}
        };
        int lastShootX = 1;
        int lastShootY = 1;

        RoundResult roundResult = winConditionChecker.getRoundResult(board, lastShootX, lastShootY);

        assertThat(roundResult).isEqualTo(WIN);
    }

    @ParameterizedTest
    @MethodSource("getContinueTestCases")
    void testContinue() {
        char[][] board = {
            {' ', ' ', ' '},
            {' ', 'x', ' '},
            {' ', ' ', ' '}
        };
        int lastShootX = 1;
        int lastShootY = 1;

        RoundResult roundResult = winConditionChecker.getRoundResult(board, lastShootX, lastShootY);

        assertThat(roundResult).isEqualTo(CONTINUE);
    }

    private static Stream<Arguments> getContinueTestCases() {
        char[][] board1 = {
                {' ', ' ', ' '},
                {' ', 'x', ' '},
                {' ', ' ', ' '}
        };

        char[][] board2 = {
                {' ', ' ', ' '},
                {' ', ' ', ' '},
                {' ', ' ', ' '}
        };

        char[][] board3 = {
                {'x', 'x', 'o'},
                {'o', 'x', 'x'},
                {'x', 'o', ' '}
        };

        char[][] board4 = {
                {'x', 'x', 'o'},
                {'o', 'x', 'x'},
                {'x', 'o', 'o'}
        };

        return Stream.of(
                Arguments.of((Object) board1),
                Arguments.of((Object) board2),
                Arguments.of((Object) board3),
                Arguments.of((Object) board4)
        );
    }

    @ParameterizedTest
    @MethodSource("getTieTestCases")
    void shouldProperlyCheckWinConditions(char[][] board, int lastShootX, int lastShootY, RoundResult expectedResult) {
        RoundResult roundResult = winConditionChecker.getRoundResult(board, lastShootX, lastShootY);

        assertThat(roundResult).isEqualTo(expectedResult);
    }

    private static Stream<Arguments> getTieTestCases() {
        List<Arguments> testCases = new ArrayList<>();

        char[][] tie1 = {
                {'x', 'x', 'o'},
                {'o', 'x', 'x'},
                {'x', 'o', 'o'}
        };
        testCases.add(Arguments.of((Object) tie1, 2, 2, TIE));

        char[][] tie2 = {
                {'x', 'x', 'o'},
                {'o', 'x', 'x'},
                {'x', 'o', 'o'}
        };
        testCases.add(Arguments.of((Object) tie2, 2, 2, TIE));

        char[][] tie3 = {
                {'x', 'x', 'o'},
                {'o', 'x', 'x'},
                {'x', 'o', 'o'}
        };
        testCases.add(Arguments.of((Object) tie3, 2, 2, TIE));

        char[][] tie4 = {
                {'x', 'x', 'o'},
                {'o', 'x', 'x'},
                {'x', 'o', 'o'}
        };
        testCases.add(Arguments.of((Object) tie4, 2, 2, TIE));

        return testCases.stream();
    }
}