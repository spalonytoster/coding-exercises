## the system

This is a small app (Java 8, Spring 2.3, Maven, H2 in-memory DB) that holds the information about our customers and their orders and exposes them through REST.

## business case

Our marketing department wants to introduce a new promotion for our best customers.
These customers will be presented with a special promotion via phone by the marketing department.

Unfortunately it came to our attention that some of our customers are fraudulent.
We don't want to present them with the promotion because there is a high risk that they will not need our product during their imminent jail-time.
It's quite convenient that the last sprint your team have delivered integration with the FPB (Fraudulent People Bureau). 

## your task

Please create a report that lists our 5 best customers.

* exposed as a REST (the architect said so)
* the best customers have the most orders
* only orders above 50$ should be included
* exclude fraudulent customers

## the solution

The agreed solution is to:
1. Get all customers ordered from the best to the worst (using native SQL query).
1. Knock-out the fraudulent customers in the service layer and then return the first 5 good customers.

## an immersion breaking note

We're interested in seeing how do you approach coding, tasks and requirements.
So please approach this task as you professionally normally do.
Talk out loud and explain why you do the things that you do.
Your questions are also important to us so please do not hesitate to ask them.
The exercise assumes that you are a programmer in a team, so we will gladly answer any questions.

## writing SQL

During the exercise we will ask you to write some SQL code.
Normally one would connect to an existing DB and use code completion in the IDE to help oneself and get immediate feedback.
Connecting to an inmemory H2 DB (running inside an application) through a JDBC is practically impossible.
So there are some ways you can help yourself with writing SQL.

1. Connect to an H2 web console.
When the application starts please search for this string in the application logs:
`H2 console available at '/h2-console'. Database available at`
In that log message a JDBC URL will be provided.
It can be used to access the currently running H2 DB through  the H2 web console:
 `http://localhost:8080/h2-console`
1. Another option is to use an existing DB and import in it the contents of the `db.sql` file.
All the required tables and data are included in it.
1. One can also import the `db.sql` file in the `http://sqlfiddle.com/` and use that to write the SQL.
While you will have the benefit of the immediate feedback no code completion will be available.