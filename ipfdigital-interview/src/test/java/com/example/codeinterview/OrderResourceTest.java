package com.example.codeinterview;

import com.example.codeinterview.OrderResource.PlaceOrderResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.ResultActions;

import java.time.LocalDate;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class OrderResourceTest extends BaseSpringMvcTest {

    @Autowired
    private OrderRepository orders;

    @Test
    @DisplayName("orders can be placed")
    void placeOrders() throws Exception {
        // given
        int clientId = 1;
        int price = 16;

        // when
        ResultActions result = mockMvc.perform(post("/orders/place/")
                .param("clientId", "" + clientId)
                .param("price", "" + price));

        // then
        result.andExpect(status().isCreated())
                .andExpect(content().contentType(APPLICATION_JSON));

        // then
        String jsonString = result.andReturn().getResponse().getContentAsString();
        PlaceOrderResponse order = new ObjectMapper().readValue(jsonString, PlaceOrderResponse.class);
        assert order.clientId == clientId: "bad client id";
        assert order.price == price: "bad value";

        // then
        Order orderFromDb = this.orders.findById(order.id).get();
        assert orderFromDb.getDate().compareTo(LocalDate.now()) == 0;
        assert order.issued.compareTo(LocalDate.now()) == 0;
    }
}