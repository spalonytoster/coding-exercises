package com.example.codeinterview;

import org.springframework.data.repository.CrudRepository;

interface OrderRepository extends CrudRepository<Order, Integer> {
}
