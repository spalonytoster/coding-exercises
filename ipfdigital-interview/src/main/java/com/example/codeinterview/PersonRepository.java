package com.example.codeinterview;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

interface PersonRepository extends CrudRepository<Person, Integer> {

    @Query(value = "select * from person where regexp_like(name, :startWith || '*')", nativeQuery = true)
    List<Person> personsWithNamesStartingWith(String startWith);
}
