package com.example.codeinterview;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
class PersonService {

    private PersonRepository theObject;

    @Autowired
    void setTheObject(PersonRepository repo) {
        theObject = repo;
    }

    public List<Person> getAllPersones() {
        List<Person> l = new ArrayList<>(); Iterator<Person> i = theObject.findAll().iterator();
        while(i.hasNext()) l.add(i.next());
        return l;
    }
}
