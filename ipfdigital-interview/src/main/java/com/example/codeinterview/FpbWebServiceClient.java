package com.example.codeinterview;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.stream.IntStream;

import static java.util.concurrent.Executors.newSingleThreadScheduledExecutor;
import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.stream.Collectors.toList;

/**
 *
 * TOP SECRET implementation
 *
 * You are not allowed to change this code during the exercise.
 *
 */
@Service
public class FpbWebServiceClient {

    private final ScheduledExecutorService executorService = newSingleThreadScheduledExecutor();
    private final Random random = new Random();
    private final long delay;

    FpbWebServiceClient() {
        this.delay = random.nextInt(9) + 1;
    }

    FpbWebServiceClient(int delay) {
        this.delay = delay;
    }

    public CompletableFuture<FraudulentPeopleReport> downloadFraudulentPeopleReport() {
        CompletableFuture<FraudulentPeopleReport> future = new CompletableFuture<>();
        executorService.schedule(() -> {
            future.complete(createReport());
        }, delay, SECONDS);
        return future;
    }

    private FraudulentPeopleReport createReport() {
        List<PersonStatus> statuses = IntStream.rangeClosed(1, 10000)
                .mapToObj(this::createPersonStatus)
                .collect(toList());

        return new FraudulentPeopleReport(statuses);
    }

    private PersonStatus createPersonStatus(int personId) {
        boolean status = personId % 2 == 0;
        return new PersonStatus(personId, status);
    }

    public static class FraudulentPeopleReport {

        private final List<PersonStatus> statuses;

        public FraudulentPeopleReport(List<PersonStatus> statuses) {
            this.statuses = statuses;
        }

        public List<PersonStatus> getStatuses() {
            return statuses;
        }
    }

    public static class PersonStatus {

        private final int id;
        private final boolean fraudulent;

        public PersonStatus(int id, boolean fraudulent) {
            this.id = id;
            this.fraudulent = fraudulent;
        }

        public int getId() {
            return id;
        }

        public boolean isFraudulent() {
            return fraudulent;
        }
    }
}
