package com.example.codeinterview;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("orders")
public class OrderResource {

    private final OrderService service;

    @Autowired
    public OrderResource(OrderService service) {
        this.service = service;
    }

    @PostMapping("place")
    ResponseEntity<PlaceOrderResponse> place(
            @RequestParam(value = "clientId", required = true) Integer clientId,
            @RequestParam(value = "price", required = true) Integer price) {
        Order order = service.placeNewOrder(clientId, price);
        PlaceOrderResponse response = new PlaceOrderResponse(order);
        return new ResponseEntity<>(response, CREATED);
    }

    @JsonRootName("order")
    public static class PlaceOrderResponse {

        public int id;
        public int clientId;
        public int price;
        public LocalDate issued;

        @JsonCreator
        public PlaceOrderResponse(
                @JsonProperty("id") int id,
                @JsonProperty("clientId") int clientId,
                @JsonProperty("price") int price,
                @JsonProperty("issued") String issued) {
            this.id = id;
            this.clientId = clientId;
            this.price = price;
            this.issued = LocalDate.parse(issued);
        }

        public PlaceOrderResponse(Order order) {
            this.id = order.getId();
            this.clientId = order.getPerson();
            this.price = order.getPrice();
            this.issued = order.getDate();
        }
    }
}
