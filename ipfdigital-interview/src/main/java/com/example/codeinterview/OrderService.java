package com.example.codeinterview;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
class OrderService {

    private final OrderRepository repository;

    @Autowired
    OrderService(OrderRepository repository) {
        this.repository = repository;
    }

    public Order placeNewOrder(int clientId, int price) {
        Order order = new Order(clientId, price, LocalDate.now());
        return repository.save(order);
    }
}
