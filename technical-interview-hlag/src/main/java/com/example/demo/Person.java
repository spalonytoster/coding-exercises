package com.example.demo;

public record Person(long id,
        String name,
        String gender,
        Integer age) {
}