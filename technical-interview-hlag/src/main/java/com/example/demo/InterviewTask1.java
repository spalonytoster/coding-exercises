package com.example.demo;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

// TODO
// 1. Create class Person with fields: id, name, gender and age (data types are up to you)
// 2. Use parameter sourceUserList and create collection that contains all persons with unique IDs.
// 3. Group collection of Persons by gender (K is for female, M is for male)
// 4. Display grouped collections in following format:
// (Gender) [sorted list of names] {Average age of all persons in the group - this is optional}

public class InterviewTask1 {
    static List<String> input = List.of("KAROLINA,K,22", "ANNA,K,32", "PAWEŁ,M,41", "KAMIL,M,26", "AGATA,K,19", "MATEUSZ,M,35", "MAGDA,K,29", "JAKUB,M,24", "EWA,K,31", "DAWID,M,40", "NATALIA,K,25", "ROBERT,M,38", "KATARZYNA,K,22", "GRZEGORZ,M,30", "JOANNA,K,35", "MARCIN,M,28", "PAULINA,K,24", "TOMASZ,M,37");

//                    "KAROLINA,      K,  22",
//                    "ANNA,          K,  32",
//                    "PAWEŁ,         M,  41",
//                    "KAMIL,         M,  26",
//                    "AGATA,         K,  19",
//                    "MATEUSZ,       M,  35",
//                    "MAGDA,         K,  29",
//                    "JAKUB,         M,  24",
//                    "EWA,           K,  31",
//                    "DAWID,         M,  40",
//                    "NATALIA,       K,  25",
//                    "ROBERT,        M,  38",
//                    "KATARZYNA,     K,  22",
//                    "GRZEGORZ,      M,  30",
//                    "JOANNA,        K,  35",
//                    "MARCIN,        M,  28",
//                    "PAULINA,       K,  24",
//                    "TOMASZ,        M,  37");

    public static void main(String[] args) {
        interviewTask(input);
    }

    private static void interviewTask(List<String> sourceUserList) {
        AtomicLong idGenerator = new AtomicLong(0L);

        Map<String, List<Person>> groupedByGender = sourceUserList.stream()
                .map(personString -> mapToPerson(personString, idGenerator))
                .sorted(Comparator.comparing(Person::name))
                .collect(Collectors.groupingBy(Person::gender));

        groupedByGender.forEach((key, value) -> System.out.printf("(%s) [%s] {%f}\n",
                key,
                getAggregatedNames(value),
                getAverageAge(value)));
    }

    private static String getAggregatedNames(List<Person> persons) {
        String[] array = persons.stream()
                .map(Person::name)
                .toArray(String[]::new);

        return String.join(", ", array);
    }

    private static double getAverageAge(List<Person> persons) {
        return persons.stream()
                .mapToInt(Person::age)
                .average()
                .orElseThrow();
    }

    private static Person mapToPerson(String personString, AtomicLong idGenerator) {
        String[] split = personString.split(",");

        String name = split[0];
        String gender = split[1];
        Integer age = Integer.valueOf(split[2]);

        return new Person(idGenerator.incrementAndGet(), name, gender, age);
    }
}