package com.example.demo;

import org.springframework.util.DigestUtils;

import java.util.Base64;

public class PathShortener {
    public final Base64.Encoder encoder = Base64.getEncoder();

    public String generateShortenedPath(String path) {
        byte[] md5Digest = DigestUtils.md5Digest(path.getBytes());
        return encoder.encodeToString(encoder.encode(md5Digest));
//        return UUID.randomUUID().toString();
    }
}
