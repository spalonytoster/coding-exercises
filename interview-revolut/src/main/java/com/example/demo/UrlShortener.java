package com.example.demo;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class UrlShortener {
    public static final String SHORT_HOST = "www.rev.me";
    public static final int CACHE_LIMIT = 100;

    private final Map<String, String> cache = new HashMap<>();
    private final PathShortener pathShortener = new PathShortener();

    public URL shortenUrl(URL url) throws MalformedURLException {
        // check if we already have the input key
        if (cache.size() >= CACHE_LIMIT) {
            throw new RuntimeException("Limit exceeeded!");
        }

        URL shortened =
                new URL("https", SHORT_HOST, pathShortener.generateShortenedPath(url.getPath()));

        cache.put(url.toString(), shortened.toString());

        return shortened;
    }
}
