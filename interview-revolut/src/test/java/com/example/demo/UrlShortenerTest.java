package com.example.demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class UrlShortenerTest {
    private UrlShortener urlShortener;

    @BeforeEach
    void setup() {
        urlShortener = new UrlShortener();
    }

    @Test
    void shouldShortenUrl() throws MalformedURLException {
        String inputUrl = "https://www.revolut.com/rewards-personalised-cashback-and-discounts/";
        URL url = new URL(inputUrl);

        URL result = urlShortener.shortenUrl(url);

        assertThat(result)
                .isNotNull();
    }

    @Test
    void shouldThrowExceptionWhenLimitExceeded() throws MalformedURLException {
        for (int i = 0; i < UrlShortener.CACHE_LIMIT; i++) {
            urlShortener.shortenUrl(new URL("https://www.revolut.com/" + UUID.randomUUID()));
        }

        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> {
            urlShortener.shortenUrl(new URL("https://www.revolut.com/" + UUID.randomUUID()));
        });
    }
}