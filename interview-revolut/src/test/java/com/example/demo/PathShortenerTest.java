package com.example.demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PathShortenerTest {

    private PathShortener pathShortener;

    @BeforeEach
    void setup() {
        pathShortener = new PathShortener();
    }

    @Test
    void generateShortenedPath() {
        String input = "/rewards-personalised-cashback-and-discounts/";

        String shortened = pathShortener.generateShortenedPath(input);

        assertThat(shortened)
                .isNotBlank();
    }
}